from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from openpyxl import Workbook

import time
import requests
import sys, getopt
import shutil
import os

arquivo_excel = Workbook()
lista_refin = arquivo_excel.active
driver = webdriver.Firefox()

_row_ = 2
i = 0
usuario = " "
senha = " "
#
#
#
# ---------------------------------------------------------------------------

def login_script(usuario, senha):
    driver.get("https://extranet.asteba.com/")

    user_box = driver.find_element_by_id("usuario")
    user_box.send_keys(usuario)

    passwd_box = driver.find_element_by_id("senha")
    passwd_box.send_keys(senha)
    time.sleep(80)

# ---------------------------------------------------------------------------
def restart_page():
    global usuario
    global senha

    is_valid()

    exit_button = driver.find_element_by_id("/Home/Deslogar")
    exit_button.click()

    driver.get("https://extranet.asteba.com/")

    login_script(usuario, senha)
# ---------------------------------------------------------------------------
def search_bot(cpf):

    time.sleep(2)
    try:
        first_button = driver.find_element_by_id("/Corretor/ConsultarContratosRenovaveis")
        first_button.click()
    except:
        search_bot(cpf)

    time.sleep(1)
    cpf_box = driver.find_element_by_id("cpf")
    cpf_box.send_keys(cpf)
    global i
    i = 0
    search_box = driver.find_element_by_id("btnConsultar")
    search_box.click()

    if is_valid() == 1:

        pagina = open("page.html", "w+")
        pagina.write(str(driver.page_source))

        URL = r"page.html"
        page = open(URL)
        soup = BeautifulSoup(page.read(), 'lxml')

        refin_data = soup.find(id='refinanciamento')

        nome = soup.find(id='nome')

        refin_data2 = soup.find(id='refinanciamento2')

        write_on(_nome_ = str(nome.text), _cpf_ = cpf, _refin_data_ = str(refin_data.text), _refin_data_2 = str(refin_data2.text))

    else:
        refin_data = "None"
        nome = "cpf invalido"
        refin_data2 = "None"

        write_on(nome, cpf, refin_data, refin_data2)

# ---------------------------------------------------------------------------
def is_valid():
    global i
    i = i+1
    time.sleep(6)
    try:
        alerta = driver.find_element_by_id("conteudoModal")
        if alerta.text == "Consulta realizada com sucesso.":
            btn_fechar = driver.find_element_by_id("btnfecharmodal")
            btn_fechar.click()
            return 1
        else:
            btn_fechar = driver.find_element_by_id("btnfecharmodal")
            btn_fechar.click()
            return 0
    except:
        if i < 5:
            time.sleep(1)
            is_valid()
        else:
            i = 0
            webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()
            return 0

# ---------------------------------------------------------------------------
def write_on(_nome_, _cpf_, _refin_data_, _refin_data_2):

    global _row_

    lista_refin.title = "Lista de refin - ASTEBA"

    lista_refin['A1'] = "Nome Beneficiario"
    lista_refin['B1'] = "CPF"
    lista_refin['C1'] = "1° AUX"
    lista_refin['D1'] = "2° AUX"

    conta = _row_ - 1
    print(conta, "\n")

    lista_refin.cell(row=_row_, column=1, value=str(_nome_))
    lista_refin.cell(row=_row_, column=2, value=str(_cpf_))
    lista_refin.cell(row=_row_, column=3, value=str(_refin_data_))
    lista_refin.cell(row=_row_, column=4, value=str(_refin_data_2))

    _row_ = _row_ + 1

    print(_nome_, "\t", _cpf_, "\t", _refin_data_, "\t", _refin_data_2)

# ---------------------------------------------------------------------------
def salvar_lista(_lista_final_):
    global _row_
    _row_ = 2
    arquivo_excel.save(_lista_final_)
    driver.close()

# ---------------------------------------------------------------------------
def start_script(lista_cpf):
    global usuario
    global senha

    hold = 0

    abertura = open('open.txt', 'r')
    print(abertura.read())

    usuario = str(input("Insira seu usuario asteba: "))
    senha = str(input("Insira sua senha asteba:    "))

    login_script(usuario, senha)

    cpf_list = open(lista_cpf)
    for cpf in cpf_list:
        search_bot(cpf)
        hold += 1
        if hold > 250:
            hold = 0
            restart_page()

# ---------------------------------------------------------------------------

def main(argv):
   inputfile = ' '
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile="])
   except getopt.GetoptError:
      print ('refinscraper.py -i <lista de cpf>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('refinscraper.py -i <lista de cpf>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg



      start_script(inputfile)

# ---------------------------------------------------------------------------

arquivo = str(input("Salvar lista em:   "))
print("\n")

main(sys.argv[1:])

print("\n")

salvar_lista(arquivo)
